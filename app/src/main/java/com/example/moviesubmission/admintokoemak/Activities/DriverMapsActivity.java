package com.example.moviesubmission.admintokoemak.Activities;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.moviesubmission.admintokoemak.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class DriverMapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private TextView lokasiTitle,jarakTitle,driverOnline;


    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext( CalligraphyContextWrapper.wrap( newBase ) );
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        CalligraphyConfig.initDefault( new CalligraphyConfig.Builder().setDefaultFontPath( "fonts/Muli.ttf" )
                .setFontAttrId( R.attr.fontPath )
                .build());
        setContentView( R.layout.activity_driver_maps );
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById( R.id.map );
        mapFragment.getMapAsync( this );
        Typeface style = Typeface.createFromAsset( getAssets(), "fonts/MuliBold.ttf" );
        lokasiTitle = findViewById( R.id.textLokasiPengiriman );
        jarakTitle = findViewById( R.id.textJarakLokasi );
        driverOnline = findViewById( R.id.driverOnline );
        lokasiTitle.setTypeface( style );
        jarakTitle.setTypeface( style );
        driverOnline.setTypeface( style );


    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng( -7.151742412469171, 111.87336690723896 );
        mMap.addMarker( new MarkerOptions().position( sydney ).title( "Bojonegoro" ) );
        mMap.moveCamera( CameraUpdateFactory.newLatLng( sydney ) );
        mMap.animateCamera( CameraUpdateFactory.zoomTo( 15 ) );
    }
}
