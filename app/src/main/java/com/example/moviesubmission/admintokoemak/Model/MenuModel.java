package com.example.moviesubmission.admintokoemak.Model;

public class MenuModel {
    String gambar;
    String menu;

    public MenuModel() {
    }

    public MenuModel(String gambar, String menu) {
        this.gambar = gambar;
        this.menu = menu;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getMenu() {
        return menu;
    }

    public void setMenu(String menu) {
        this.menu = menu;
    }
}
