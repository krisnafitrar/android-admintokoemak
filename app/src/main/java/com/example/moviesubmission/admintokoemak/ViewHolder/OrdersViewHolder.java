package com.example.moviesubmission.admintokoemak.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.moviesubmission.admintokoemak.Interface.ItemClickListener;
import com.example.moviesubmission.admintokoemak.R;

public class OrdersViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView kodeOrders,statusOrders,phoneUser,alamat,detail,cancelOrders,updateOrders;
    ItemClickListener itemClickListener;

    public OrdersViewHolder(@NonNull View itemView) {
        super( itemView );
        kodeOrders = itemView.findViewById( R.id.kodeOrders );
        statusOrders = itemView.findViewById( R.id.statusOrders );
        phoneUser = itemView.findViewById( R.id.nomorHpOrders );
        alamat = itemView.findViewById( R.id.alamatOrders );
        detail = itemView.findViewById( R.id.detailOrder );
        cancelOrders = itemView.findViewById( R.id.cancelOrder );
        updateOrders = itemView.findViewById( R.id.updateOrder );

        itemView.setOnClickListener( this );

    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {
        itemClickListener.onClick( v, getAdapterPosition(), false );
    }
}
