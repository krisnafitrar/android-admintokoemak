package com.example.moviesubmission.admintokoemak.Model;

public class KontenModel {
    String idkonten,idkontenkategori,nama,gambar,deskripsi,url;

    public KontenModel(String idkonten, String idkontenkategori, String nama, String gambar, String deskripsi, String url) {
        this.idkonten = idkonten;
        this.idkontenkategori = idkontenkategori;
        this.nama = nama;
        this.gambar = gambar;
        this.deskripsi = deskripsi;
        this.url = url;
    }

    public KontenModel() {
    }

    public String getIdkonten() {
        return idkonten;
    }

    public void setIdkonten(String idkonten) {
        this.idkonten = idkonten;
    }

    public String getIdkontenkategori() {
        return idkontenkategori;
    }

    public void setIdkontenkategori(String idkontenkategori) {
        this.idkontenkategori = idkontenkategori;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getGambar() {
        return gambar;
    }

    public void setGambar(String gambar) {
        this.gambar = gambar;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
