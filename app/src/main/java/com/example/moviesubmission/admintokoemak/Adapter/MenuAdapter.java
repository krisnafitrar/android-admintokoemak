package com.example.moviesubmission.admintokoemak.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.moviesubmission.admintokoemak.Model.MenuModel;
import com.example.moviesubmission.admintokoemak.R;

import java.util.ArrayList;

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewProcessHolder> {
    private Context mContext;
    private ArrayList<MenuModel>mList;

    public MenuAdapter(Context mContext, ArrayList<MenuModel> mList) {
        this.mContext = mContext;
        this.mList = mList;
    }

    @NonNull
    @Override
    public MenuAdapter.ViewProcessHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from( mContext ).inflate( R.layout.layout_menu,viewGroup,false );
        return new ViewProcessHolder( view );
    }

    @Override
    public void onBindViewHolder(@NonNull MenuAdapter.ViewProcessHolder viewProcessHolder, int i) {
        MenuModel menuModel = mList.get( i );
        ImageView icon;
        TextView title;
        LinearLayout btn;

        icon = viewProcessHolder.icon;
        title = viewProcessHolder.txtMenu;
        btn = viewProcessHolder.button;

        Glide.with( mContext )
                .load( menuModel.getGambar())
                .into( icon );

        title.setText( menuModel.getMenu() );

        btn.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText( mContext, "Clicked", Toast.LENGTH_SHORT ).show();
            }
        } );

    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewProcessHolder extends RecyclerView.ViewHolder {
        ImageView icon;
        TextView txtMenu;
        LinearLayout button;
        public ViewProcessHolder(@NonNull View itemView) {
            super( itemView );
            icon = itemView.findViewById( R.id.gambarIcon );
            txtMenu = itemView.findViewById( R.id.namaIcon );
            button = itemView.findViewById( R.id.linearMenu );
        }
    }
}
