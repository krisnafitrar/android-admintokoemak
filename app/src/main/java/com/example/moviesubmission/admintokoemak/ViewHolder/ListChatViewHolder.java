package com.example.moviesubmission.admintokoemak.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.example.moviesubmission.admintokoemak.Interface.ItemClickListener;
import com.example.moviesubmission.admintokoemak.R;

public class ListChatViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public TextView nomorUserChat,txtMsg;
    private ItemClickListener itemClickListener;

    public ListChatViewHolder(@NonNull View itemView) {
        super( itemView );
        nomorUserChat = itemView.findViewById( R.id.listNomorUser );
        txtMsg = itemView.findViewById( R.id.txtchat );

        itemView.setOnClickListener( this );
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View v) {
        itemClickListener.onClick( v, getAdapterPosition(), false );
    }
}
