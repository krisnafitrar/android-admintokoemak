package com.example.moviesubmission.admintokoemak.Model;

import java.util.List;

public class RequestModel {
    private String phone;
    private String nama;
    private String alamat;
    private String hargasementara;
    private String diskon;
    private String ongkir;
    private String total;
    private List<OrderModel> item;
    private String status;
    private String tanggal;
    private String jadwalPengantaran;

    public RequestModel() {
    }

    public RequestModel(String phone, String nama, String alamat, String total, String hargasementara, String diskon, String ongkir, String tanggal, String jadwal, List<OrderModel> item) {
        this.phone = phone;
        this.nama = nama;
        this.alamat = alamat;
        this.total = total;
        this.hargasementara = hargasementara;
        this.diskon = diskon;
        this.ongkir = ongkir;
        this.item = item;
        this.status = "0";
        this.tanggal = tanggal;
        this.jadwalPengantaran = jadwal;
    }

    public String getJadwalPengantaran() {
        return jadwalPengantaran;
    }

    public void setJadwalPengantaran(String jadwalPengantaran) {
        this.jadwalPengantaran = jadwalPengantaran;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getHargasementara() {
        return hargasementara;
    }

    public void setHargasementara(String hargasementara) {
        this.hargasementara = hargasementara;
    }

    public String getDiskon() {
        return diskon;
    }

    public void setDiskon(String diskon) {
        this.diskon = diskon;
    }

    public String getOngkir() {
        return ongkir;
    }

    public void setOngkir(String ongkir) {
        this.ongkir = ongkir;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public List<OrderModel> getItem() {
        return item;
    }

    public void setItem(List<OrderModel> item) {
        this.item = item;
    }
}
