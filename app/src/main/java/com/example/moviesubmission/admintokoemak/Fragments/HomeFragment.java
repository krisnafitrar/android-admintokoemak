package com.example.moviesubmission.admintokoemak.Fragments;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.moviesubmission.admintokoemak.Adapter.MenuAdapter;
import com.example.moviesubmission.admintokoemak.Adapter.NewsAdapter;
import com.example.moviesubmission.admintokoemak.App.AppController;
import com.example.moviesubmission.admintokoemak.Model.KontenModel;
import com.example.moviesubmission.admintokoemak.Model.MenuModel;
import com.example.moviesubmission.admintokoemak.Model.User;
import com.example.moviesubmission.admintokoemak.R;
import com.example.moviesubmission.admintokoemak.Util.Server;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment {
    private NewsAdapter mNewsAdapter;
    private RecyclerView recyclerView;
    private ArrayList<KontenModel>mList;

    public HomeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate( R.layout.fragment_home, container, false );

        initViews(rootView);
        initNews();

        return rootView;
    }

    private void initNews() {
        mList = new ArrayList<>();
        loadNewsHome("home_news");
        mNewsAdapter = new NewsAdapter( getActivity(),mList );
        recyclerView.setLayoutManager( new LinearLayoutManager( getActivity(), LinearLayoutManager.HORIZONTAL,false ) );
        recyclerView.setHasFixedSize( true );
        recyclerView.setAdapter( mNewsAdapter );
    }

    private void loadNewsHome(final String idkontenkategori)
    {
        String urlWithParams = Server.REST_API_KONTEN + "?idkontenkategori=" + idkontenkategori;
        StringRequest stringRequest = new StringRequest( Request.Method.GET, urlWithParams, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    System.out.println("Response" + response);
                    JSONArray jsonArray = new JSONArray( response );
                    for(int i = 0; i < jsonArray.length(); i++){
                        try{
                            JSONObject data = jsonArray.getJSONObject( i );
                            KontenModel model = new KontenModel();
                            model.setIdkonten( data.getString( "idkonten" ) );
                            model.setIdkontenkategori( data.getString( "idkontenkategori" ) );
                            model.setNama( data.getString( "nama" ) );
                            model.setGambar( data.getString( "gambar" ) );
                            model.setDeskripsi( data.getString( "deskripsi" ) );
                            model.setUrl( data.getString( "url" ) );
                            mList.add( model );
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                }catch (JSONException e){
                    e.printStackTrace();
                }
                mNewsAdapter.notifyDataSetChanged();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
                System.out.println("error : " + error.getMessage());
            }
        } );
        AppController.getInstance().addToRequestQueue(stringRequest,"json_obj_req");
    }

    private void initViews(View rootView) {
        recyclerView = rootView.findViewById( R.id.rv_menuNews );
    }

}
