package com.example.moviesubmission.admintokoemak.ViewHolder;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.moviesubmission.admintokoemak.Interface.ItemClickListener;
import com.example.moviesubmission.admintokoemak.R;


public class ItemDetailViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public ImageView gambarItem;
    public TextView namaItem,hargaItem,jumlahItem;
    private ItemClickListener itemClickListener;

    public ItemDetailViewHolder(@NonNull View itemView) {
        super( itemView );
        gambarItem = itemView.findViewById( R.id.imgCartList );
        namaItem = itemView.findViewById( R.id.namaCartList );
        hargaItem = itemView.findViewById( R.id.hargaCartList );
        jumlahItem = itemView.findViewById( R.id.jumlahCartList );
    }

    public void setItemClickListener(ItemClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    @Override
    public void onClick(View view) {
        itemClickListener.onClick( view,getAdapterPosition(), false  );
    }
}
