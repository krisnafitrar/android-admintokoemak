package com.example.moviesubmission.admintokoemak.Activities;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.moviesubmission.admintokoemak.R;
import com.google.firebase.database.FirebaseDatabase;

public class NewsActivity extends AppCompatActivity {
    private FirebaseDatabase db = FirebaseDatabase.getInstance();
    private WebView webView;
    private ImageView backArrow,reload;
    private TextView titleNews;
    private Typeface styleFont;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_news );

        styleFont = Typeface.createFromAsset( getAssets(),"fonts/Poppins-Medium.ttf" );
        webView = findViewById( R.id.webView );
        backArrow = findViewById( R.id.backArrowNews );
        reload = findViewById( R.id.reloadNews );
        titleNews = findViewById( R.id.titleNews );
        titleNews.setTypeface( styleFont );
        backArrow.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        } );


        // melakukan zoom.
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setJavaScriptEnabled( true );

        // menambahkan scrollbar dan progress bar di dalam WebView
        final ProgressBar progressBar = findViewById( R.id.progressWeb );
        progressBar.setMax( 100 );
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.loadUrl( getIntent().getStringExtra( "url" ) );
        reload.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webView.loadUrl( getIntent().getStringExtra( "url" ) );
            }
        } );
        progressBar.setProgress( 0 );
        webView.setWebViewClient(new WebViewClient(){
            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished( view, url );
                progressBar.setVisibility( View.GONE );
            }
        });

        webView.setWebChromeClient( new WebChromeClient(){
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                progressBar.setProgress( newProgress );
                super.onProgressChanged( view,newProgress );
            }
        } );



    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }
}
