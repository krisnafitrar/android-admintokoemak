package com.example.moviesubmission.admintokoemak.Model;

public class OrderModel {
    private String ProdukId,NamaProduk,Jumlah,Harga,Diskon,Gambar;

    public OrderModel() {
    }

    public OrderModel(String produkId, String namaProduk, String jumlah, String harga, String diskon, String gambar) {
        ProdukId = produkId;
        NamaProduk = namaProduk;
        Jumlah = jumlah;
        Harga = harga;
        Diskon = diskon;
        Gambar = gambar;
    }

    public String getGambar() {
        return Gambar;
    }

    public void setGambar(String gambar) {
        this.Gambar = gambar;
    }

    public String getProdukId() {
        return ProdukId;
    }

    public void setProdukId(String produkId) {
        ProdukId = produkId;
    }

    public String getNamaProduk() {
        return NamaProduk;
    }

    public void setNamaProduk(String namaProduk) {
        NamaProduk = namaProduk;
    }

    public String getJumlah() {
        return Jumlah;
    }

    public void setJumlah(String jumlah) {
        Jumlah = jumlah;
    }

    public String getHarga() {
        return Harga;
    }

    public void setHarga(String harga) {
        Harga = harga;
    }

    public String getDiskon() {
        return Diskon;
    }

    public void setDiskon(String diskon) {
        Diskon = diskon;
    }
}
