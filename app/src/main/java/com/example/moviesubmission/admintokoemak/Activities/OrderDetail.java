package com.example.moviesubmission.admintokoemak.Activities;

import android.content.Context;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.moviesubmission.admintokoemak.Interface.ItemClickListener;
import com.example.moviesubmission.admintokoemak.Model.OrderModel;
import com.example.moviesubmission.admintokoemak.Model.RequestModel;
import com.example.moviesubmission.admintokoemak.R;
import com.example.moviesubmission.admintokoemak.ViewHolder.ItemDetailViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class OrderDetail extends AppCompatActivity {
    private Toolbar toolbar;
    private FirebaseDatabase db = FirebaseDatabase.getInstance();
    private DatabaseReference reference = db.getReference("request");
    private String kodeBelanja;
    private TextView nomorPesanan,statusPesanan,tglPesanan,namaPemesan,nomorTelepon,alamatPemesan,estimasiHarga,diskon,ongkir,total;
    private TextView teks1,teks2,teks3,teks4,teks5,teks6,teks7,teks8,teks9,teks10,teks11,teks12;
    private RecyclerView recyclerView;
    private FirebaseRecyclerOptions<OrderModel> options;
    private FirebaseRecyclerAdapter<OrderModel, ItemDetailViewHolder> adapter;
    private TextView tanggalPengantaran;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext( CalligraphyContextWrapper.wrap( newBase ) );
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        CalligraphyConfig.initDefault( new CalligraphyConfig.Builder().setDefaultFontPath( "fonts/Muli.ttf" )
                .setFontAttrId( R.attr.fontPath )
                .build());
        setContentView( R.layout.activity_order_detail );

        //init view and set typeface
        Typeface style = Typeface.createFromAsset( getAssets(), "fonts/MuliBold.ttf" );
        final Typeface styleku = Typeface.createFromAsset( getAssets(), "fonts/MuliBold.ttf" );
        teks1 = findViewById( R.id.teks1 );
        teks2 = findViewById( R.id.teks2 );
        teks3 = findViewById( R.id.teks3 );
        teks4 = findViewById( R.id.teks4 );
        teks5 = findViewById( R.id.teks5 );
        teks6 = findViewById( R.id.teks6 );
        teks7 = findViewById( R.id.teks7 );
        teks8 = findViewById( R.id.teks8 );
        teks9 = findViewById( R.id.teks9 );
        teks10 = findViewById( R.id.teks10 );
        teks11 = findViewById( R.id.teks11 );
        teks12 = findViewById( R.id.teks12 );

        teks1.setTypeface( style );
        teks2.setTypeface( style );
        teks3.setTypeface( style );
        teks4.setTypeface( style );
        teks5.setTypeface( style );
        teks6.setTypeface( style );
        teks7.setTypeface( style );
        teks8.setTypeface( style );
        teks9.setTypeface( style );
        teks10.setTypeface( style );
        teks11.setTypeface( style );
        teks12.setTypeface( style );


        //init view
        nomorPesanan = findViewById( R.id.nomorPesananDetail );
        statusPesanan = findViewById( R.id.statusPesananDetail );
        tglPesanan = findViewById( R.id.tanggalPesananDetail );
        namaPemesan = findViewById( R.id.namaPemesanDetail );
        nomorTelepon = findViewById( R.id.nomorHandphoneDetail );
        alamatPemesan = findViewById( R.id.alamatDetail );
        tanggalPengantaran = findViewById( R.id.tanggalPengantaran );
        estimasiHarga = findViewById( R.id.estimasiHargaDetail );
        diskon = findViewById( R.id.diskonDetail );
        ongkir = findViewById( R.id.ongkirDetail );
        total = findViewById( R.id.totalDetail );
        toolbar = findViewById( R.id.toolbarOrderDetail );
        recyclerView = findViewById( R.id.rv_OrderDetail );
        recyclerView.setNestedScrollingEnabled( false );
        setSupportActionBar( toolbar );
        toolbar.setNavigationIcon( R.drawable.ic_keyboard_arrow_left_black_24dp );
        toolbar.setNavigationOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        } );

        //get Intent
        if(getIntent() != null){
            kodeBelanja = getIntent().getStringExtra( "kode" );
        }

        loadItem(kodeBelanja);

        options = new FirebaseRecyclerOptions.Builder<OrderModel>()
                .setQuery( reference.child( kodeBelanja ).child( "item" ), OrderModel.class ).build();

        adapter = new FirebaseRecyclerAdapter<OrderModel, ItemDetailViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull ItemDetailViewHolder holder, int position, @NonNull OrderModel model) {
                Glide.with(OrderDetail.this)
                        .load( model.getGambar() )
                        .into( holder.gambarItem );
                holder.namaItem.setTypeface( styleku );
                holder.namaItem.setText( model.getNamaProduk() );
                holder.hargaItem.setText( "Rp." + model.getHarga() );
                holder.jumlahItem.setText( "Jumlah : " + model.getJumlah() );

                holder.setItemClickListener( new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        Toast.makeText( OrderDetail.this, "List Order", Toast.LENGTH_SHORT ).show();
                    }
                } );

            }

            @NonNull
            @Override
            public ItemDetailViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from( viewGroup.getContext() ).inflate( R.layout.cart_layout, viewGroup, false );
                return new ItemDetailViewHolder( view );
            }
        };

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager( this );
        RecyclerView.LayoutManager rvLilayout = linearLayoutManager;
        recyclerView.setLayoutManager( rvLilayout );
        adapter.startListening();
        recyclerView.setAdapter( adapter );

    }

    private void loadItem(final String kodeBelanja) {
        reference.addValueEventListener( new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot.child( kodeBelanja ).exists()){
                    RequestModel requestModel = dataSnapshot.child( kodeBelanja ).getValue(RequestModel.class);
                    nomorPesanan.setText( "ORD" + kodeBelanja );
                    statusPesanan.setText( convertStatus(requestModel.getStatus()) );
                    tglPesanan.setText( requestModel.getTanggal() );
                    namaPemesan.setText( requestModel.getNama() );
                    nomorTelepon.setText( requestModel.getPhone() );
                    alamatPemesan.setText( requestModel.getAlamat() );
                    tanggalPengantaran.setText( requestModel.getJadwalPengantaran() );
                    estimasiHarga.setText( requestModel.getHargasementara() );
                    diskon.setText( requestModel.getDiskon() );
                    ongkir.setText( requestModel.getOngkir() );
                    total.setText( requestModel.getTotal() );
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        } );
    }

    private String convertStatus(String status) {
        if(status.equals( "0" ))
            return "Request";
        else if(status.equals( "1" ))
            return "Dikemas";
        else if(status.equals( "2" ))
            return "Dikirim";
        else if (status.equals( "3" ))
            return "Diterima";
        else
            return "Dibatalkan";
    }
}
