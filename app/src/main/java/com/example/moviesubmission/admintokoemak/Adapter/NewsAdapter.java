package com.example.moviesubmission.admintokoemak.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.moviesubmission.admintokoemak.Activities.NewsActivity;
import com.example.moviesubmission.admintokoemak.Model.KontenModel;
import com.example.moviesubmission.admintokoemak.R;

import java.util.ArrayList;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.ViewProcessHolder> {
    private Context mContext;
    private ArrayList<KontenModel> mList;
    private boolean isClicked = false;

    public NewsAdapter(Context mContex, ArrayList<KontenModel> mList) {
        this.mContext = mContex;
        this.mList = mList;
    }

    @NonNull
    @Override
    public NewsAdapter.ViewProcessHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from( mContext ).inflate( R.layout.news_layout,viewGroup,false );
        ViewProcessHolder viewProcessHolder = new ViewProcessHolder( view );

        return viewProcessHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull NewsAdapter.ViewProcessHolder viewProcessHolder, int i) {
        final KontenModel newsModel = mList.get( i );
        CardView cardNews;
        final ImageView imageNews;
        TextView news,newsTitle,newsAuthor;

        cardNews = viewProcessHolder.cardView;
        imageNews = viewProcessHolder.imageView;
        news = viewProcessHolder.news;
        newsTitle = viewProcessHolder.title;
        newsAuthor = viewProcessHolder.author;

        Typeface style = Typeface.createFromAsset( mContext.getAssets(),"fonts/Poppins-Medium.ttf" );
        news.setTypeface( style );
        newsAuthor.setTypeface( style );

        Glide.with( mContext )
                .load(newsModel.getGambar())
                .into( imageNews );

        newsTitle.setText( newsModel.getDeskripsi() );
        newsAuthor.setText( newsModel.getNama() );

        cardNews.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent( mContext,NewsActivity.class );
                intent.putExtra( "url", newsModel.getUrl() );
                mContext.startActivity( intent );
            }
        } );
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ViewProcessHolder extends RecyclerView.ViewHolder {
        CardView cardView;
        ImageView imageView;
        TextView news,title,author;

        public ViewProcessHolder(@NonNull View itemView) {
            super( itemView );
            cardView = itemView.findViewById( R.id.cardNews );
            imageView = itemView.findViewById( R.id.imgNews );
            news = itemView.findViewById( R.id.namaNews );
            title = itemView.findViewById( R.id.newsTitle );
            author = itemView.findViewById( R.id.newsAuthor );
        }
    }
}
