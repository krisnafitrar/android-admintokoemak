package com.example.moviesubmission.admintokoemak.Service;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.example.moviesubmission.admintokoemak.Activities.MainActivity;
import com.example.moviesubmission.admintokoemak.Model.RequestModel;
import com.example.moviesubmission.admintokoemak.R;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ListenOrder extends Service implements ChildEventListener {
    FirebaseDatabase db;
    DatabaseReference requestReference;
    MediaPlayer mediaPlayer;

    public ListenOrder() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        db = FirebaseDatabase.getInstance();
        requestReference = db.getReference( "request" );
        mediaPlayer = MediaPlayer.create( getBaseContext(),R.raw.sound );
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        requestReference.addChildEventListener( this );
        return super.onStartCommand( intent, flags, startId );

    }

    @Override
    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
        RequestModel requestModel = dataSnapshot.getValue(RequestModel.class);
        mediaPlayer.start();
        showNotification(dataSnapshot.getKey(),requestModel);
    }

    private void showNotification(String key, RequestModel requestModel) {
        String channelId = "tokoemak";
        Intent intent = new Intent( getBaseContext(), MainActivity.class );
        PendingIntent contentIntent = PendingIntent.getActivity(getBaseContext(),0,intent,PendingIntent.FLAG_UPDATE_CURRENT);

        NotificationCompat.Builder builder = new NotificationCompat.Builder( getBaseContext(), channelId);
        builder.setAutoCancel( true )
                .setDefaults( Notification.DEFAULT_ALL )
                .setWhen( System.currentTimeMillis() )
                .setTicker("POS Tokoemak")
                .setContentTitle( "Request Order" )
                .setContentText( "Yeay ada request baru nih TM-" + key )
                .setContentIntent( contentIntent )
                .setPriority( Notification.PRIORITY_MAX )
                .setSmallIcon( R.drawable.adminlogo );





        NotificationManager notificationManager = (NotificationManager) getBaseContext().getSystemService( Context.NOTIFICATION_SERVICE );
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
        {
            NotificationChannel channel = new NotificationChannel(
                    channelId,
                    "easy shopping",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
            builder.setChannelId(channelId);
        }


        notificationManager.notify(0, builder.build());
    }

    @Override
    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//        RequestModel requestModel = dataSnapshot.getValue(RequestModel.class);
//        mediaPlayer.start();
//        showNotification(dataSnapshot.getKey(),requestModel);
    }

    @Override
    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

    }

    @Override
    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }

}
