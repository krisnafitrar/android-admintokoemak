package com.example.moviesubmission.admintokoemak.Model;

public class ListChatModel {
    String key;

    public ListChatModel() {
    }

    public ListChatModel(String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
