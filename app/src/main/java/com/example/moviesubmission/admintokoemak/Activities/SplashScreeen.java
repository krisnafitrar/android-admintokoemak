package com.example.moviesubmission.admintokoemak.Activities;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import com.example.moviesubmission.admintokoemak.R;

public class SplashScreeen extends AppCompatActivity {
    private TextView titleSplash;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_splash_screeen );

        // In Activity's onCreate() for instance
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window w = getWindow();
            w.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS);
        }

        titleSplash = findViewById( R.id.titleSplash );
        Typeface style = Typeface.createFromAsset( getAssets(), "fonts/neo.ttf" );
        titleSplash.setTypeface( style );

        Thread timer = new Thread(){
            @Override
            public void run() {
                try {
                    sleep( 3000 );
                }catch (Exception e){
                    e.printStackTrace();
                }finally {
                    startActivity( new Intent( SplashScreeen.this,MainActivity.class ) );
                    finish();
                }
            }
        };

        timer.start();
    }
}
