package com.example.moviesubmission.admintokoemak.Fragments;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.moviesubmission.admintokoemak.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccountFragment extends Fragment {
    private TextView titleToolbar;
    private Typeface fBold;


    public AccountFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment\
        View rootView = inflater.inflate( R.layout.fragment_account, container, false );

        titleToolbar = rootView.findViewById( R.id.textToolbarAkun );
        fBold = Typeface.createFromAsset( getActivity().getAssets(),"fonts/Poppins-Medium.ttf" );
        titleToolbar.setTypeface( fBold );

        return rootView;
    }

}
