package com.example.moviesubmission.admintokoemak.Activities;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;

import com.example.moviesubmission.admintokoemak.Fragments.AccountFragment;
import com.example.moviesubmission.admintokoemak.Fragments.ChatFragment;
import com.example.moviesubmission.admintokoemak.Fragments.HomeFragment;
import com.example.moviesubmission.admintokoemak.Fragments.OrdersFragment;
import com.example.moviesubmission.admintokoemak.R;
import com.example.moviesubmission.admintokoemak.Service.ListenOrder;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity {
    BottomNavigationView bottomNavigationView;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext( CalligraphyContextWrapper.wrap( newBase ) );
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        CalligraphyConfig.initDefault( new CalligraphyConfig.Builder().setDefaultFontPath( "fonts/Muli.ttf" )
                .setFontAttrId( R.attr.fontPath )
                .build());
        setContentView( R.layout.activity_main );

        //init view
        bottomNavigationView = findViewById( R.id.bottomNavbar );


        final HomeFragment homeFragment = new HomeFragment();
        final OrdersFragment ordersFragment = new OrdersFragment();
        final ChatFragment chatFragment = new ChatFragment();
        final AccountFragment accountFragment = new AccountFragment();

        bottomNavigationView.setOnNavigationItemSelectedListener( new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                int id = menuItem.getItemId();

                if(id == R.id.homeNav){
                    setFragment(homeFragment);
                    return true;
                }else if(id == R.id.ordersNav){
                    setFragment(ordersFragment);
                    return true;
                }else if(id == R.id.chatNav){
                    setFragment( chatFragment );
                    return true;
                }else if(id == R.id.accountNav){
                    setFragment( accountFragment );
                    return true;
                }

                return false;
            }
        } );

        bottomNavigationView.setSelectedItemId( R.id.homeNav );

        Intent service = new Intent( MainActivity.this, ListenOrder.class );
        startService( service );

    }

    private void setFragment(Fragment fragment) {
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace( R.id.frameLayout, fragment );
        fragmentTransaction.commit();
    }


}
