package com.example.moviesubmission.admintokoemak.Fragments;


import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.moviesubmission.admintokoemak.Activities.ChatActivity;
import com.example.moviesubmission.admintokoemak.Interface.ItemClickListener;
import com.example.moviesubmission.admintokoemak.Model.ChatModel;
import com.example.moviesubmission.admintokoemak.Model.ListChatModel;
import com.example.moviesubmission.admintokoemak.R;
import com.example.moviesubmission.admintokoemak.ViewHolder.ListChatViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChatFragment extends Fragment {
    private FirebaseDatabase db;
    private DatabaseReference reference;
    private FirebaseRecyclerOptions<ListChatModel>options;
    private FirebaseRecyclerAdapter<ListChatModel,ListChatViewHolder> adapter;
    private RecyclerView recyclerView;
    private TextView titleToolbar;
    private Typeface fBold;

    public ChatFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate( R.layout.fragment_chat, container, false );

        db = FirebaseDatabase.getInstance();
        reference = db.getReference( "chat" );
        titleToolbar = rootView.findViewById( R.id.textToolbarChat );
        fBold = Typeface.createFromAsset( getActivity().getAssets(),"fonts/Poppins-Medium.ttf" );
        titleToolbar.setTypeface( fBold );

        //init view
        recyclerView = rootView.findViewById( R.id.rv_list_chat );

        options = new FirebaseRecyclerOptions.Builder<ListChatModel>()
                        .setQuery(reference,ListChatModel.class).build();

        adapter = new FirebaseRecyclerAdapter<ListChatModel, ListChatViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull final ListChatViewHolder holder, int position, @NonNull ListChatModel model) {
                holder.nomorUserChat.setText( adapter.getRef( position ).getKey() );
                holder.nomorUserChat.setTypeface( fBold );
                holder.setItemClickListener( new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {
                        Intent intent = new Intent( getActivity(),ChatActivity.class );
                        intent.putExtra( "nomor",adapter.getRef( position ).getKey() );
                        startActivity( intent );
                    }
                } );

                reference.child( adapter.getRef( position ).getKey() ).limitToLast( 1 ).addChildEventListener( new ChildEventListener() {
                    @Override
                    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
                        ChatModel chatModel = dataSnapshot.getValue( ChatModel.class );
                        holder.txtMsg.setText( setMsg(chatModel.getMessage()));
                    }

                    @Override
                    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                    }

                    @Override
                    public void onCancelled(@NonNull DatabaseError databaseError) {

                    }
                } );

            }

            @NonNull
            @Override
            public ListChatViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from( viewGroup.getContext() ).inflate( R.layout.list_chat,viewGroup,false );
                return new ListChatViewHolder(view);
            }
        };

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager( getActivity() );
        RecyclerView.LayoutManager rvLayoutManager = linearLayoutManager;
        recyclerView.setLayoutManager( rvLayoutManager );
        recyclerView.setNestedScrollingEnabled( false );
        recyclerView.addItemDecoration( new DividerItemDecoration( recyclerView.getContext(),DividerItemDecoration.VERTICAL ) );
        adapter.startListening();
        recyclerView.setAdapter( adapter );

        return rootView;
    }

    private String setMsg(String message) {
        if(message.length() > 32){
            return message.substring( 0,32 ) + " ...";
        }else{
            return message;
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if(adapter != null){
            adapter.startListening();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(adapter != null){
            adapter.startListening();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if(adapter != null){
            adapter.stopListening();
        }
    }
}
