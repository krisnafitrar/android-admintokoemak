package com.example.moviesubmission.admintokoemak.Activities;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.moviesubmission.admintokoemak.Interface.ItemClickListener;
import com.example.moviesubmission.admintokoemak.Model.ChatModel;
import com.example.moviesubmission.admintokoemak.R;
import com.example.moviesubmission.admintokoemak.ViewHolder.ChatViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ChatActivity extends AppCompatActivity {
    FirebaseDatabase db;
    DatabaseReference reference;
    String nomorUser;
    RecyclerView recyclerView;
    FirebaseRecyclerOptions<ChatModel> options;
    FirebaseRecyclerAdapter<ChatModel,ChatViewHolder> adapter;
    ImageButton btnSend;
    EditText editText;
    Toolbar toolbar;
    TextView textTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_chat );

        if(getIntent() != null){
            nomorUser = getIntent().getStringExtra( "nomor" );
        }

        //init firebase
        db = FirebaseDatabase.getInstance();
        reference = db.getReference( "chat" ).child( nomorUser );

        //init view
        recyclerView = findViewById( R.id.rv_chat );
        btnSend = findViewById( R.id.btnSendMessage );
        editText = findViewById( R.id.messageUser );
        toolbar = findViewById( R.id.toolbarChat );
        textTitle = findViewById( R.id.nomorUserChat );

        

        textTitle.setText( nomorUser );

        btnSend.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(editText.getText().toString().equals( "" )){
                    Toast.makeText( ChatActivity.this, "Isi pesannya!", Toast.LENGTH_SHORT ).show();
                }else{
                    ChatModel chatModel = new ChatModel( "Admin",editText.getText().toString(),getWaktu() );
                    reference.child( String.valueOf( System.currentTimeMillis() ) )
                            .setValue( chatModel );
                    editText.setText( "" );
                }
            }
        } );

        loadRecylerView();

    }

    private void loadRecylerView() {
        options = new FirebaseRecyclerOptions.Builder<ChatModel>()
                .setQuery( reference,ChatModel.class ).build();

        adapter = new FirebaseRecyclerAdapter<ChatModel, ChatViewHolder>(options) {
            @Override
            protected void onBindViewHolder(@NonNull ChatViewHolder holder, int position, @NonNull ChatModel model) {
                //set value leftside
                holder.username.setText( model.getUsername() );
                holder.message.setText( model.getMessage() );
                holder.time.setText( model.getTime() );

                //set value rightside
                holder.usernameRight.setText( model.getUsername() );
                holder.messageRight.setText( model.getMessage() );
                holder.timeRight.setText( model.getTime() );


                if(!holder.username.getText().toString().equals( "Admin" )){
                    //set invisible chat kanan
                    holder.username.setVisibility( View.GONE );
                    holder.cardChatRight.setVisibility( View.GONE );
                    holder.usernameRight.setVisibility( View.GONE );
                    holder.messageRight.setVisibility( View.GONE );
                    holder.timeRight.setVisibility( View.GONE );
                }else{
                    //set invisible chat kiri
                    holder.usernameRight.setVisibility( View.GONE );
                    holder.cardChat.setVisibility( View.GONE );
                    holder.username.setVisibility( View.GONE );
                    holder.message.setVisibility( View.GONE );
                    holder.time.setVisibility( View.GONE );
                }


                holder.setItemClickListener( new ItemClickListener() {
                    @Override
                    public void onClick(View view, int position, boolean isLongClick) {

                    }
                } );
            }

            @NonNull
            @Override
            public ChatViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
                View view = LayoutInflater.from( viewGroup.getContext() ).inflate( R.layout.layout_chat,viewGroup,false );
                return new ChatViewHolder( view );
            }
        };

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager( this );
        RecyclerView.LayoutManager rvLayoutManager = linearLayoutManager;
        recyclerView.setLayoutManager( rvLayoutManager );
        recyclerView.setNestedScrollingEnabled( false );
        adapter.startListening();
        recyclerView.setAdapter( adapter );
    }

    private String getWaktu(){
        DateFormat dateFormat = new SimpleDateFormat( "HH:mm" );
        Date date = new Date();
        return dateFormat.format( date );
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(adapter != null){
            adapter.startListening();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(adapter != null){
            adapter.startListening();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(adapter != null){
            adapter.stopListening();
        }
    }
}
