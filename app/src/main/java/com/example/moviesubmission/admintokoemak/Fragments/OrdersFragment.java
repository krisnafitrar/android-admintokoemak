package com.example.moviesubmission.admintokoemak.Fragments;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.bumptech.glide.request.RequestOptions;
import com.example.moviesubmission.admintokoemak.Activities.DriverMapsActivity;
import com.example.moviesubmission.admintokoemak.Activities.OrderDetail;
import com.example.moviesubmission.admintokoemak.App.AppController;
import com.example.moviesubmission.admintokoemak.Interface.ItemClickListener;
import com.example.moviesubmission.admintokoemak.Model.RequestModel;
import com.example.moviesubmission.admintokoemak.R;
import com.example.moviesubmission.admintokoemak.Util.Server;
import com.example.moviesubmission.admintokoemak.ViewHolder.OrdersViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.jaredrummler.materialspinner.MaterialSpinner;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import cn.pedant.SweetAlert.SweetAlertDialog;


public class OrdersFragment extends Fragment {
    private FirebaseDatabase db;
    private DatabaseReference reference;
    private FirebaseRecyclerOptions<RequestModel> options;
    private FirebaseRecyclerAdapter<RequestModel, OrdersViewHolder> adapter;
    private RecyclerView recyclerView;
    private Typeface style;
    public RecyclerView.LayoutManager layoutManager;
    private MaterialSpinner materialSpinner;
    private TextView titleToolbar,txtJumlah;

    public OrdersFragment(){
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate( R.layout.fragment_orders, container, false );



     //init view
     recyclerView = rootView.findViewById( R.id.rv_listOrders );
     style = Typeface.createFromAsset( getActivity().getAssets(), "fonts/Poppins-Medium.ttf" );
     db = FirebaseDatabase.getInstance();
     reference = db.getReference("request");
     titleToolbar = rootView.findViewById( R.id.textToolbarOrder );
     txtJumlah = rootView.findViewById( R.id.txtJumlahOrder );

     titleToolbar.setTypeface( style );
     txtJumlah.setTypeface( style );


    options = new FirebaseRecyclerOptions.Builder<RequestModel>()
            .setQuery( reference, RequestModel.class ).build();

    adapter = new FirebaseRecyclerAdapter<RequestModel, OrdersViewHolder>(options) {
        @Override
        protected void onBindViewHolder(@NonNull OrdersViewHolder holder, final int position, @NonNull final RequestModel model) {
              txtJumlah.setText( "Jumlah order : " + adapter.getItemCount() );
              holder.kodeOrders.setTypeface( style );
              holder.kodeOrders.setText( "TM-" + adapter.getRef( position ).getKey() );
              holder.phoneUser.setText( model.getPhone() );
              holder.statusOrders.setText( convertStatus(model.getStatus()) );
              holder.alamat.setText( convertAlamat(model.getAlamat()) );

              holder.detail.setOnClickListener( new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                      Intent intent = new Intent( getActivity(), OrderDetail.class );
                      intent.putExtra( "kode",adapter.getRef( position ).getKey() );
                      startActivity( intent );
                  }
              } );

              holder.cancelOrders.setOnClickListener( new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                      new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                              .setTitleText("Apakah anda yakin?")
                              .setContentText("Pesanan akan di reject")
                              .setConfirmText("Reject order!")
                              .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                  @Override
                                  public void onClick(SweetAlertDialog sDialog) {
                                      reference.child( adapter.getRef( position ).getKey() ).child( "status" ).setValue( "4" );
                                      sDialog
                                              .setTitleText("Rejected")
                                              .setContentText("Pesanan user telah di reject")
                                              .setConfirmText("OKE")
                                              .setConfirmClickListener(null)
                                              .changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                                  }
                              })
                              .show();
                  }
              } );

              holder.setItemClickListener( new ItemClickListener() {
                  @Override
                  public void onClick(View view, int position, boolean isLongClick) {
                      Toast.makeText( getActivity(), "Barang sedang " + convertStatus( model.getStatus() ), Toast.LENGTH_SHORT ).show();
                  }
              } );

              holder.updateOrders.setOnClickListener( new View.OnClickListener() {
                  @Override
                  public void onClick(View v) {
                      final AlertDialog.Builder alertDialog = new AlertDialog.Builder( getActivity() );
                      alertDialog.setTitle( "Update Order" );
                      alertDialog.setMessage( "Ubah status pesanan" );

                      LayoutInflater inflaterNew = getActivity().getLayoutInflater();
                      final View view = inflaterNew.inflate( R.layout.update_status, null );
                      materialSpinner = view.findViewById( R.id.updateSpinner );
                      materialSpinner.setItems("Diproses","Dikemas","Dikirim","Diterima","Dibatalkan");
                      alertDialog.setView( view );

                      alertDialog.setPositiveButton( "SET", new DialogInterface.OnClickListener() {
                          @Override
                          public void onClick(DialogInterface dialog, int which) {
                              dialog.dismiss();
                              reference.child( adapter.getRef( position ).getKey() ).child( "status" ).setValue( String.valueOf( materialSpinner.getSelectedIndex() ));
                              updateStatusPesanan(materialSpinner.getSelectedIndex() + 1,adapter.getRef( position ).getKey());
                              new SweetAlertDialog(getActivity(), SweetAlertDialog.SUCCESS_TYPE)
                                      .setTitleText("Update successful!")
                                      .setContentText("Berhasil update pesanan")
                                      .show();
                          }
                      } );

                      alertDialog.setNegativeButton( "CANCEL", new DialogInterface.OnClickListener() {
                          @Override
                          public void onClick(DialogInterface dialog, int which) {
                              dialog.dismiss();
                          }
                      } );

                      alertDialog.show();

                  }
              } );

        }

        @NonNull
        @Override
        public OrdersViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
            View view = LayoutInflater.from( viewGroup.getContext() ).inflate( R.layout.ongoingorder_layout, viewGroup, false );
            return new OrdersViewHolder(view);
        }
    };

        recyclerView.setNestedScrollingEnabled( false );
        layoutManager = new LinearLayoutManager( getContext() );
        recyclerView.setLayoutManager( layoutManager );
        adapter.startListening();
        adapter.notifyDataSetChanged();
        recyclerView.setAdapter( adapter );

        return rootView;
    }

    private void updateStatusPesanan(final int i, final String key) {
        StringRequest stringRequest = new StringRequest( Request.Method.POST, Server.REST_API_TRANSAKSI, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONObject jsonObject = new JSONObject( response );
                    if(jsonObject.getBoolean( "status" )){
                        System.out.println("Berhasil update " + i);
                    }else{
                        System.out.println("Gagal Update " + i);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                error.printStackTrace();
            }
        } ){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                Map<String,String>params = new HashMap<>();
                params.put( "global_act","updatestatus" );
                params.put("idstatus",String.valueOf( i ));
                params.put("kodetrans",key);
                if(i == 5){
                    params.put( "params","dibatalkanpada" );
                }else if(i == 4){
                    params.put( "params","diterimapada" );
                }
                return params;
            }
        };
        AppController.getInstance().addToRequestQueue( stringRequest,"json_obj_req" );
    }

    private String convertAlamat(String alamat) {
        if(alamat.length() >= 25){
            return alamat.substring( 0,25 ) + " ...";
        }else{
            return alamat;
        }

    }

    private String convertStatus(String status) {
        if(status.equals( "0" ))
            return "Request";
        else if(status.equals( "1" ))
            return "Dikemas";
        else if(status.equals( "2" ))
            return "Dikirim";
        else if (status.equals( "3" ))
            return "Diterima";
        else
            return "Dibatalkan";
    }

    @Override
    public void onStart() {
        super.onStart();
        if(adapter != null){
            adapter.startListening();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(adapter != null){
            adapter.startListening();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if(adapter != null){
            adapter.stopListening();
        }
    }
}
